#include "function-strategy.hpp"
#include "algorithm.hpp"
#include "ns3/core-module.h"

#include "model/ndn-l3-protocol.hpp"
#include "model/ndn-app-link-service.hpp"
#include "model/null-transport.hpp"

#include "ns3/ndnSIM/model/ndn-common.hpp"
#include "ns3/ndnSIM/model/ndn-app-link-service.hpp"
#include "ns3/ndnSIM/NFD/daemon/face/face.hpp"

#include <ndn-cxx/lp/tags.hpp>

#include "ns3/application.h"
#include "ns3/ptr.h"
#include "ns3/callback.h"
#include "ns3/traced-callback.h"
#include "ns3/node-list.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/topology-read-module.h"
#include "ns3/ndnSIM/helper/ndn-global-routing-helper.hpp"

#include <ndn-cxx/mgmt/nfd/control-command.hpp>
#include <ndn-cxx/mgmt/nfd/control-parameters.hpp>
#include <ndn-cxx/mgmt/nfd/command-options.hpp>
#include "ns3/ndnSIM/helper/ndn-stack-helper.hpp"

namespace nfd {
NFD_LOG_INIT("FunctionStrategy");
namespace fw {

#define FLOOD_RANGE 0

const time::nanoseconds FunctionStrategy::MEASUREMENTS_LIFETIME = time::minutes(
		60);
//const time::nanoseconds FunctionStrategy::INTEREST_LIFETIME = time::seconds(60);

const Name FunctionStrategy::STRATEGY_NAME(
		"ndn:/localhost/nfd/strategy/function/%FD%01");
NFD_REGISTER_STRATEGY(FunctionStrategy);

FunctionStrategy::FunctionStrategy(Forwarder& forwarder, const Name& name) :
		Strategy(forwarder, name) {
}

void FunctionStrategy::afterReceiveInterest(const Face& inFace,
		const Interest& interest, const shared_ptr<pit::Entry>& pitEntry) {
	Name func_prefix("/exec");
	if (!func_prefix.isPrefixOf(interest.getName())) {
		NFD_LOG_DEBUG(
				"Got an interest with a wrong prefix" << interest.getName());
		this->rejectPendingInterest(pitEntry);
		return;
	}

	shared_ptr<ndn::lp::HopCountTag> hopCountTag = interest.getTag<
			ndn::lp::HopCountTag>();
	int hopCount = 0;
	if (hopCountTag != NULL) {
		hopCount = *hopCountTag;
	}

	if (interest.getDiscover() == 1 && hopCount > FLOOD_RANGE) {
		NS_LOG_DEBUG("Discover = 0 - dropping");
		this->rejectPendingInterest(pitEntry);
		return;
	}

	NFD_LOG_DEBUG(
			"Found a function to execute: " << interest.getName() << " taskSize: " << interest.getTaskSize() << " deadline: " << interest.getTaskDeadline() << " discover: " << interest.getDiscover() << " hop count: " << hopCount);

	const fib::Entry& fibEntry = this->lookupFib(*pitEntry);
	const fib::NextHopList& nexthops = fibEntry.getNextHops();

	const fib::Entry* cloudFibEntry = m_forwarder.getFib().findExactMatch(
			Name("/cloud"));
	BOOST_ASSERT(cloudFibEntry != nullptr);
	const fib::NextHopList& cloudNextHops = cloudFibEntry->getNextHops();
	fib::NextHopList::const_iterator cit = cloudNextHops.begin();
	NS_LOG_DEBUG("Face towards the cloud: " << cit->getFace().getId());
	FaceId cloudFaceId = cit->getFace().getId();

	FaceId advertiseFaceId = 0;
	const fib::Entry* advertiseFibEntry = m_forwarder.getFib().findExactMatch(
			interest.getName().getSubName(1).getPrefix(-1));
	if (advertiseFibEntry == nullptr) {
		NS_LOG_DEBUG("No PREFIX entry for" << interest.getName());
	} else {
		const fib::NextHopList& advertiseNextHops =
				advertiseFibEntry->getNextHops();
		fib::NextHopList::const_iterator ait = advertiseNextHops.begin();
		NS_LOG_DEBUG(
				"Face towards the advertising node: " << ait->getFace().getId());
		advertiseFaceId = ait->getFace().getId();
	}

	m_forwarder.m_ks.printFib();
	m_forwarder.m_ks.printLocalPrefixes();

	FaceId mainFaceId = 0;
	if (advertiseFaceId) {
		mainFaceId = advertiseFaceId;
	} else {
		mainFaceId = cloudFaceId;
	}

	FaceId faceId;
	struct Container delayStat;
	delayStat.name = interest.getName();
	delayStat.time = ns3::Simulator::Now();

	if ((faceId = m_forwarder.m_ks.getBestFace(inFace.getId(), interest))) {
		NS_LOG_DEBUG("Got id from KS: " << faceId);
		Face* outFace = m_forwarder.getFace(faceId);
		BOOST_ASSERT(outFace != nullptr);

		NS_LOG_DEBUG(
				"Sending from KS on: (" << outFace->getId() << ") " << outFace);
		delayStat.faceId = outFace->getId();
		m_delays.push_back(delayStat);
		ns3::Simulator::Schedule(ns3::Seconds(3),
				&nfd::fw::FunctionStrategy::clean, this, delayStat);

		this->sendInterest(pitEntry, *outFace, interest);
		m_forwarder.m_ks.updateFace(outFace->getId());
		m_forwarder.m_ks.manageFacePopularity(interest, m_sentPackets++,
				outFace->getId());

	} else {
		if (!interest.getDiscover()) {
			delayStat.faceId = mainFaceId;
			Face* mainFace = m_forwarder.getFace(mainFaceId);
			BOOST_ASSERT(mainFace != nullptr);
			NS_LOG_DEBUG("Sending the original packet on " << mainFaceId);
			m_forwarder.m_ks.manageFacePopularity(interest, m_sentPackets++,
					mainFaceId);
			m_delays.push_back(delayStat);
			ns3::Simulator::Schedule(ns3::Seconds(3),
					&nfd::fw::FunctionStrategy::clean, this, delayStat);
			this->sendInterest(pitEntry, *mainFace, interest);
		}

		for (fib::NextHopList::const_iterator it = nexthops.begin();
				it != nexthops.end(); ++it) {
			Face& outFace = it->getFace();
			if (!wouldViolateScope(inFace, interest, outFace)
					&& canForwardToLegacy(*pitEntry, outFace)
					&& (outFace.getId() != mainFaceId)
					&& (hopCount < FLOOD_RANGE)) {

				NS_LOG_DEBUG("Sending a discovery packet on " << outFace);
				delayStat.faceId = outFace.getId();
				m_delays.push_back(delayStat);
				ns3::Simulator::Schedule(ns3::Seconds(3),
						&nfd::fw::FunctionStrategy::clean, this, delayStat);
				sendDiscoverPackets(interest, outFace);

			}
		}

	}
}

void FunctionStrategy::beforeSatisfyInterest(
		const shared_ptr<pit::Entry>& pitEntry, const Face& inFace,
		const Data& data) {
	NS_LOG_FUNCTION_NOARGS();
	std::list<struct Container>::iterator it;
	for (it = m_delays.begin(); it != m_delays.end(); it++) {
		if (it->name.equals(data.getName()) && (it->faceId == inFace.getId())) {
			ns3::Time delay = ns3::Simulator::Now() - it->time;
			NS_LOG_DEBUG(
					"Data received on (" << inFace.getId() << ") delay(ms): " << delay.GetMilliSeconds() << " faceDetails: " << inFace.getId());
			m_forwarder.m_ks.manageDelay(data.getName(), inFace.getId(), delay);
			m_delays.erase(it);
			return;
		}

	}
}

void FunctionStrategy::beforeExpirePendingInterest(
		const shared_ptr<pit::Entry> & pitEntry) {
	NS_LOG_DEBUG("!!!!!!!!!!!!!!!!!!!!!!EXPIRE!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

	std::list<struct Container>::iterator it;
	for (it = m_delays.begin(); it != m_delays.end(); it++) {
		if (it->name.equals(pitEntry->getName())) {
			ns3::Time delay = ns3::Simulator::Now() - it->time;
			NS_LOG_DEBUG(
					"Data expiring on (" << it->faceId << ") delay(ms): " << delay.GetMilliSeconds() << " faceDetails: " << it->faceId);
			m_forwarder.m_ks.manageDelay(pitEntry->getName(), it->faceId,
					delay);
			m_delays.erase(it);
			return;
		}

	}
}

void FunctionStrategy::beforeUnsolicited(const Face& inFace, const Data& data) {
	NS_LOG_DEBUG(
			"!!!!!!!!!!!!!!!!!!!!!!UNSOLICITED!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	std::list<struct Container>::iterator it;
	for (it = m_delays.begin(); it != m_delays.end(); it++) {
		if (it->name.equals(data.getName()) && (it->faceId == inFace.getId())) {
			ns3::Time delay = ns3::Simulator::Now() - it->time;
			NS_LOG_DEBUG(
					"Data received on (" << inFace.getId() << ") delay(ms): " << delay.GetMilliSeconds() << "faceDetails: " << inFace.getId());
			m_forwarder.m_ks.manageDelay(data.getName(), inFace.getId(), delay);
			m_delays.erase(it);
			return;
		}

	}
}

void FunctionStrategy::sendDiscoverPackets(const Interest& oldInterest,
		Face& face) {

	shared_ptr<Name> name = make_shared<Name>(oldInterest.getName());
	shared_ptr<Interest> interest = make_shared<Interest>(oldInterest);
	interest->setName(*name);
	interest->setNonce(22);
	interest->setDiscover(1);
	interest->setTaskDeadline(oldInterest.getTaskDeadline());
	interest->setTaskSize(oldInterest.getTaskSize());
	shared_ptr<pit::Entry> pitEntry =
			m_forwarder.getPit().insert(*interest).first;
	NFD_LOG_DEBUG(
			"Sending discovery packet: " << interest->getName() << " taskSize: " << interest->getTaskSize() << " deadline: " << interest->getTaskDeadline() << " discover: " << interest->getDiscover());

	this->sendInterest(pitEntry, face, *interest);
}

void FunctionStrategy::clean(struct Container stats) {
	std::list<struct Container>::iterator it;
	for (it = m_delays.begin(); it != m_delays.end(); it++) {
		if (it->name.equals(stats.name) && (it->faceId == stats.faceId)
				&& (it->time == stats.time)) {
			m_delays.erase(it);
			return;
		}

	}
}

void FunctionStrategy::onData(Data& data) {
	NFD_LOG_DEBUG("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");NS_LOG_FUNCTION_NOARGS();
}

void FunctionStrategy::afterInterest(Interest& interest) {
	NFD_LOG_DEBUG(interest.getName());NS_LOG_FUNCTION_NOARGS();
}

} // namespace fw
} // namespace nfd

