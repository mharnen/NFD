#ifndef NFD_DAEMON_FW_FACE_INFO_HPP
#define NFD_DAEMON_FW_FACE_INFO_HPP


#include "ns3/random-variable-stream.h"
#include "ns3/ndnSIM/model/ndn-common.hpp"


namespace nfd{
namespace ks{


class FaceInfo {
public:


  FaceInfo();
  FaceInfo(FaceId faceId);
  void
  updateDelay(unsigned int delay) const;

  FaceId
  getFaceId()const {
	  return m_faceId;
  }

  void
  updatePopularity(const unsigned long packetCounter, FaceId faceId) const;

  FaceId m_faceId;
  mutable unsigned int m_sumDelay = 0;
  mutable unsigned int m_delayNum = 0;
  mutable unsigned int m_load = 0;
  mutable bool m_overloaded = false;
  mutable std::list<int> m_popularity;
  mutable unsigned long m_lastPacket = 0;
  static const unsigned int PERIOD_SIZE = 3;
  static const unsigned int POP_SIZE = 10;

};

std::ostream &operator<<(std::ostream &os, nfd::ks::FaceInfo m);

} //namespace fw
} //namespace nfd

#endif // NFD_DAEMON_FW_FACE_INFO_HPP
