#ifndef NFD_DAEMON_FW_KERNEL_INFO_HPP
#define NFD_DAEMON_FW_KERNEL_INFO_HPP


#include "ns3/random-variable-stream.h"
#include "ns3/ndnSIM/model/ndn-common.hpp"
#include "face-kernel-info.hpp"

namespace nfd{
namespace ks{


class KernelInfo {
public:
  static constexpr int
  getTypeId(){
    return 1000;
  }
  Name getName() const{
    return m_name;
  }
  struct compare{
	  bool operator() (const nfd::ks::KernelInfo k1, const nfd::ks::KernelInfo k2) const{
		      return k1.getName().toUri() > k2.getName().toUri();
	}
  };

  KernelInfo(Name name);
  KernelInfo(const Interest interest);
  KernelInfo(Name name, double deadline);
  void
  updateDelay(unsigned int delay, FaceId faceId) const;
  void
  addFace(FaceId faceId) const;

  void updatePopularity(const unsigned long packetCounter, Name name, int hopCount) const;
  void updateFacePopularity(FaceId faceId) const;
  double calculateScore() const;


  void
  faceExpired(FaceId faceId) const;
  void 
  updatePrefFaces(const unsigned long packetCounter, FaceId faceId);

  double getDeadline() const{
	  return m_deadline;
  }

  int getTaskType() const{
	  return m_taskType;
  }

  void setTaskType(int taskType) const{
	  m_taskType = taskType;
  }

  FaceId getNextFace() const;

public:
  mutable unsigned long lastPacket = 0;
  mutable std::list<int> popularity;
  mutable std::list<FaceKernelInfo> m_prefFaces;
  mutable double m_deadline = 0.0;
  mutable double m_taskSize = 0.0;
  
  mutable int delayIntroduced = 0;

  mutable int m_taskType;
  Name m_name;
  mutable int m_size;
  mutable unsigned int m_packetCounter = 0;
/*  unsigned int m_sumDelay = 0;
  unsigned int m_sentNum = 0;*/

  static const unsigned int POP_SIZE = 10;
  static const unsigned int PERIOD_SIZE = 3;

  mutable int m_hopCountSum = 0;
  mutable int m_hopCountNum = 0;
  mutable bool m_stored = false;
};

std::ostream &operator<<(std::ostream &os, nfd::ks::KernelInfo m);

} //namespace fw
} //namespace nfd

#endif // NFD_DAEMON_FW_KERNEL_INFO_HPP
